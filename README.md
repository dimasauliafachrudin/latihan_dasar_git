# Belajar di SIB

> Berbinar bersama Binar

File ini dibuat untuk melatih kemampuan untuk menggunakan git. Yang menjadi topik pembelajaran adalah pembuatan repo, pull, push dan branches.

```js
console.log("hallo gitLab");
```

Belajar buat markdown melalui [link ini 🤗](https://www.markdownguide.org/basic-syntax/#code)

---

Made with 💖 by Dimas Aulia
